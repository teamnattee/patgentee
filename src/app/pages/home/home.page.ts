import { Component } from '@angular/core';
import { Item } from 'src/app/entity/Item';
import { Categoria } from 'src/app/entity/Categoria';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  itens: Array<Item>;

  constructor(private router: Router) {
    
  }

  atualizaItem(item: Item, indexArrayStorage){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        item: JSON.stringify(item),
        indexArrayStorage
      }
    };

    this.router.navigate(['/form'],navigationExtras);
  }

  ionViewWillEnter() {
    this.itens = new Array<Item>();

    let itensStorage = localStorage.getItem('ItensStorage');
    if(!itensStorage) return;

    JSON.parse(itensStorage).forEach(item => {
      this.itens.push(new Item(item.nome,new Categoria(item.categoria.nome),item.descricao));
    });
  }

  deletaItem(indexArrayStorage){
    let itensFiltered = this.itens.filter((value,index) => index !== indexArrayStorage);

    localStorage.setItem('ItensStorage',JSON.stringify(itensFiltered));

    this.ionViewWillEnter();
    
  }

}
