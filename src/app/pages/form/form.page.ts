import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/entity/Categoria';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Item } from 'src/app/entity/Item';

@Component({
  selector: 'app-form',
  templateUrl: './form.page.html',
  styleUrls: ['./form.page.scss'],
})
export class FormPage implements OnInit {

  categorias: Array<Categoria>;

  formularioItem: FormGroup;

  itemToUpdate: Item;
  indexArrayStorage: number;

  constructor(private route: ActivatedRoute, private router: Router) {

    this.route.queryParams.subscribe(params => {
      if (params && params.item && params.indexArrayStorage) {
        this.itemToUpdate = JSON.parse(params.item);
        this.indexArrayStorage = params.indexArrayStorage;

        this.criaFormularioPreenchido();
        console.log(this.itemToUpdate);
      }else{
        this.criaFormularioVazio();
      }
    });

    this.categorias = new Array<Categoria>();
    this.categorias.push(new Categoria("Sapato"));
    this.categorias.push(new Categoria("Bolsa"));
    this.categorias.push(new Categoria("CD"));
    this.categorias.push(new Categoria("Livro"));

  }

  criaFormularioPreenchido(){
    this.formularioItem = new FormGroup({
      nome: new FormControl(this.itemToUpdate.nome),
      categoria: new FormControl(this.itemToUpdate.categoria.nome),
      descricao: new FormControl(this.itemToUpdate.descricao)
    });
  }
  criaFormularioVazio(){
    this.formularioItem = new FormGroup({
      nome: new FormControl(''),
      categoria: new FormControl(''),
      descricao: new FormControl('')
    });
  }

  salvaItem(){
    let itens = this.createArrayItens();

    const novoItem = new Item(this.formularioItem.value.nome,new Categoria(this.formularioItem.value.categoria),this.formularioItem.value.descricao);
    
    if(!this.itemToUpdate){
      itens.push(novoItem);
    }else{
      itens[this.indexArrayStorage] = novoItem;
    }

    localStorage.setItem('ItensStorage',JSON.stringify(itens));

    this.router.navigate(['/home']);
  }

  createArrayItens(){
    let itensStorage = localStorage.getItem('ItensStorage');
    if(itensStorage)
      return JSON.parse(itensStorage);
    else
      return [];
  }

  ngOnInit() {
  }

}
