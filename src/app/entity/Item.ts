import { Categoria } from './Categoria';

export class Item{
    constructor(public nome: string, public categoria: Categoria,public descricao: string){}
}